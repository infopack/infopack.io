import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  win = window as any;
  constructor() { }

  ngOnInit() {
  }

  goto(url: string) {
    this.win.location = url;
  }

}
